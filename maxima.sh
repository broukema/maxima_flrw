#!/bin/bash

# An open source package for the Latex typesetting of the results of
# Maxima code and tensor calculus
# (C) Szymon Sikora, September 2018, CC-BY-4.0
# - materials released during the "Inhomogeneous Cosmologies III" conference

# This work is licensed under the Creative Commons Attribution 4.0
# International License. To view a copy of this license, visit
# http://creativecommons.org/licenses/by/4.0/ or send a letter to
# Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.

# 2018-09-19 - minor edits, B. Roukema

echo -e "The name of the program: \c"
read nazwa
maxima -b ${nazwa}".mac"
cp -pv "body.tex" ${nazwa}
cd ${nazwa}
pdflatex program.tex
